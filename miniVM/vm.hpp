#pragma once
#include <vector>
#include <iostream>

typedef int32_t i32;

class vm {
private:
	i32 sp;
	i32 pc;
	std::vector<i32> memory;

public:
	void run();

};